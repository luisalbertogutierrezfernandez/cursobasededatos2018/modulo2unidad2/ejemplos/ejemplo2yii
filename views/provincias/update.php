<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Provincias */

$this->title = 'Actualizar Provincia: ' . $model->provincia;
$this->params['breadcrumbs'][] = ['label' => 'Provincias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->provincia, 'url' => ['view', 'id' => $model->provincia]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="provincias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
